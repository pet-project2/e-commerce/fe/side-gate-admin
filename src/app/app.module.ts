import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component';
import { HeaderComponent } from './layouts/full-layout/header/header.component';
import {FullLayoutModule} from "./layouts/full-layout/full-layout.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FullLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
