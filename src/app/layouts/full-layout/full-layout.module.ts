import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {FullLayoutComponent} from "./full-layout.component";
import {HeaderComponent} from "./header/header.component";
import {RouterOutlet} from "@angular/router";
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [FullLayoutComponent, HeaderComponent, FooterComponent],
  imports: [
    CommonModule,
    RouterOutlet,
    NgOptimizedImage
  ]
})
export class FullLayoutModule { }
