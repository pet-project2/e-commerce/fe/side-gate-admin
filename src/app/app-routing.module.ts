import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FullLayoutComponent} from "./layouts/full-layout/full-layout.component";
import {FULL_LAYOUT_ROUTES} from "./shared/router/full-layout.routes";

const routes: Routes = [
  {
    path: '', redirectTo: 'admin/product', pathMatch: 'full'
  },
  {
    path: 'admin', component: FullLayoutComponent, children: FULL_LAYOUT_ROUTES
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
