import {Routes} from "@angular/router";

export const FULL_LAYOUT_ROUTES: Routes = [
  {
    path: 'product',
    loadChildren: () => import('../../pages/product/product.module').then(m => m.ProductModule)
  }
];
